/*global QUnit:false, module:false, test:false, asyncTest:false, expect:false*/
/*global start:false, stop:false ok:false, equal:false, notEqual:false, deepEqual:false*/
/*global notDeepEqual:false, strictEqual:false, notStrictEqual:false, raises:false*/
(function($) {

    /*
    ======== A Handy Little QUnit Reference ========
    http://docs.jquery.com/QUnit

    Test methods:
        expect(numAssertions)
        stop(increment)
        start(decrement)
    Test assertions:
        ok(value, [message])
        equal(actual, expected, [message])
        notEqual(actual, expected, [message])
        deepEqual(actual, expected, [message])
        notDeepEqual(actual, expected, [message])
        strictEqual(actual, expected, [message])
        notStrictEqual(actual, expected, [message])
        raises(block, [expected], [message])
    */

//    module('jQuery#awesome', {
//        setup: function() {
//            this.elems = $('#qunit-fixture').children();
//        }
//    });

    test('LCD is defined', function() {
        expect(1);
        notEqual(typeof LCD, 'undefined', 'LCD function is not defined');
    });

    test('LCD number 1 displays as LCD', function() {
        var expected = [
            '...',
            '..|',
            '..|'
        ];
        equal(LCD(1), expected.join('\n'), '1 is correct')
    });

    test('LCD number 11 displays as LCD', function() {
        var expected = [
            '... ...',
            '..| ..|',
            '..| ..|'
        ];
        equal(LCD(11), expected.join('\n'), '11 is correct')
    });

    test('LCD number 111 displays as LCD', function() {
        var expected = [
            '... ... ...',
            '..| ..| ..|',
            '..| ..| ..|'
        ];
        equal(LCD(111), expected.join('\n'), '111 is correct')
    });

}(jQuery));
