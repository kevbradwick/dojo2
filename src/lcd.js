/*
 * dojo_cardiff
 * 
 *
 * Copyright (c) 2012 Kevin Bradwick
 * Licensed under the MIT license.
 */

(function($){

    this.LCD = function(number) {

        function join(strings) {
            return strings.join('\n');
        }

        function getDisplayFor(number) {
            return [
                '...',
                '..|',
                '..|'
            ];
        }

        function concat(digits) {
            result = [
                '',
                '',
                ''
            ]
            $.each(digits, function(digitindex, digit) {
                $.each(digit, function(lineindex, digitline) {
                    result[lineindex] += " " + digit[lineindex];
                });
            });
            $.each(result, function(index, line) {
                result[index] = line.trim();
            });
            return result;
        }

        numbers = number.toString().split('');
        args = numbers.map(getDisplayFor);

        result = concat(args);

        return join(result);
    }

}).call(this, jQuery);
